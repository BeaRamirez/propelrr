package BubbleSort;

import java.util.Arrays;

public class BubbleSortImplem {

	public static void main(String[] args) {
		BubbleSort bubbleSort = new BubbleSort(new int[] {7,5,12,15,19,17});
		
		bubbleSort.bubbleSortArray();
		
		System.out.println("Sorted Array:" + Arrays.toString(bubbleSort.getArray()));
		System.out.println("Median: " + bubbleSort.getMedian());
		System.out.println("Max Number: " + bubbleSort.getMax());
	}

}
