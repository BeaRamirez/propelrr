package BubbleSort;

public class BubbleSort {
	int[] arrSort;
	
	public BubbleSort(int[] _arrSort) {
		this.arrSort = _arrSort;
	}
	
	public void setArray(int[] _arrSort) {
		this.arrSort = _arrSort;
	}
	
	public int[] getArray() {
		return this.arrSort;
	}
	
	public int[] bubbleSortArray() {
		int[] arr = this.arrSort;
		int arrSize = arr.length;
		int arrSizeMax = arrSize - 1;
		
		for(int j=0; j<arrSize; j++) {
			for(int i=0; i<arrSizeMax; i++) {
				int temp = 0;
				if(arr[i] > arr[i+1]) {
					temp = arr[i];
					arr[i] = arr[i+1];
					arr[i+1] = temp;
				}
			}
		}			
		
		
		return arr;
	}
	
	public double getMedian() {
		double med = 0.0;
		int index = 0;
		
		int[] arr = bubbleSortArray();
		int arrSize = arr.length;
		
		if(arrSize % 2 != 0) {
			index = arrSize / 2;
			med = arr[index];
		}else {
			index = arrSize / 2;
			med = (arr[index-1] + arr[index]) / 2;
		}
		
		return med;
	}
	
	public int getMax() {
		return bubbleSortArray()[bubbleSortArray().length-1];
	}
}