public class Fibonacci {
	public static void main(String[] args) {
		computeFibonacci(Integer.valueOf(args[0]));
	}
	
	public static void computeFibonacci(int number) {
		double dFibonacci = 1.618034;
		double val = 0.0;
		int i = 0;
		
		if(number < 1 || number > 20) {
			System.out.println("Input must be from 1-20 only.");
		}else {
			val = (Math.pow(dFibonacci, number) - Math.pow(1 - dFibonacci, number)) / Math.sqrt(5);
			i = (int) val;
			
			System.out.println("The Fibonacci value is " + i + ".");
		}				
	}
}
